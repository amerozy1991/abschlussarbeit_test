#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QObject>
#include <QDebug>
#include <QString>
#include <QtMath>
#include <QHBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QSplineSeries>
#include <qcustomplot.h>
#include <auflagerreaktion.h>

using namespace std;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //auflagerreaktion *a= new auflagerreaktion();
    //a->show();
    //connect(a,&auflagerreaktion::backToLogIn,this,&MainWindow::back_to_mainwindwo);


    //QLineEdit *radius = new QLineEdit();
    //QLineEdit *hoehe = new QLineEdit();
    //QLineEdit *breite = new QLineEdit();
    //QLineEdit *laenge = new QLineEdit();

}


//"->itemData(ui->querschnitt_txt->currentIndex())"

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    this->hide();
    newauflagerreaktion = new auflagerreaktion();
    newauflagerreaktion->show();
    connect(newauflagerreaktion,&auflagerreaktion::backToLogIn,this,&MainWindow::back_to_mainwindwo);

}
void MainWindow::back_to_mainwindwo()
{

    this->show();
    newauflagerreaktion->close();
    //qDebug()<<"backtomainwindwo";
}
