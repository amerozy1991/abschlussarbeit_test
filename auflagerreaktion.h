#ifndef AUFLAGERREAKTION_H
#define AUFLAGERREAKTION_H

#include <QWidget>
#include <QDialog>

namespace Ui {
class auflagerreaktion;
}

class auflagerreaktion : public QDialog//public QWidget
{
    Q_OBJECT

public:
    explicit auflagerreaktion(QWidget *parent = nullptr);
    ~auflagerreaktion();
    QList<QString> Querschnitt;
signals:
   void backToLogIn();

private slots:
    void on_pushButton_clicked();
    //void on_back_pushButton_clicked();
    void cbIndexChanged();


private:
    Ui::auflagerreaktion *ui;
     int flaeche (int a,int b,int r);
};

#endif // AUFLAGERREAKTION_H
