# abschlussarbeit_test

It's a prototype for my thesis, used technology QT, programming language c++, Platform windows

Table of Contents

Development
Setup
Contribute


Development


QT
this app is being developed using QT version 5.15.1.


Setup

Make sure you have QT version 5.15.1 (or above) installed.

youiu will need the the first experince with qcustomplot




Contribute

If you wish to contribute, please use the following guidelines:

Every fix/feature/task should have its own branch.
Create a merge Request against the master branch.
When it gets approved, you can merge your Changes.



## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for the project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork the project or volunteer to step in as a maintainer or owner, allowing the project to keep going.
