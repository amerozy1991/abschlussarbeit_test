#include "auflagerreaktion.h"
#include "ui_auflagerreaktion.h"
#include "mainwindow.h"


auflagerreaktion::auflagerreaktion(QWidget *parent) :
    /*QWidget(parent)*/QDialog(parent),
    ui(new Ui::auflagerreaktion)
{
    ui->setupUi(this);
    Querschnitt.append(" ");
    Querschnitt.append("kreis");
    Querschnitt.append("quadrat");
    Querschnitt.append("vierecke");
    ui->querschnitt_txt->addItems(Querschnitt);
    connect(ui->querschnitt_txt, SIGNAL(currentIndexChanged(int)), this, SLOT(cbIndexChanged()));
    ui->laenge_txt->setValidator( new QIntValidator(0, 100, this) );
    ui->breite_txt->setValidator( new QIntValidator(0, 100, this) );
    ui->hoehe_txt->setValidator( new QIntValidator(0, 100, this) );
    ui->radius_txt->setValidator( new QIntValidator(0, 100, this) );

    QPushButton *btnOk = new QPushButton("Ok");
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(btnOk);
    this->setLayout(layout);
    connect(ui->back_pushButton, &QPushButton::clicked, this, &auflagerreaktion::backToLogIn);


}

auflagerreaktion::~auflagerreaktion()
{
    delete ui;
}

void auflagerreaktion::cbIndexChanged()
{
    int index = ui->querschnitt_txt->currentIndex();

    QString text = ui->querschnitt_txt->currentText();
    ui->label_6->setText(text);

    QLabel *_kreis;
    QLabel *_quadrat;
    QLabel *_vierecke;

    if ( text == "kreis")
    { // -1 for not found
       _kreis = new QLabel(this);
       _kreis->setText("Random String");
       _kreis->show();

       //QHBoxLayout *layout = new QHBoxLayout();

       //layout->addWidget(_kreis);
       //setLayout(layout);

//       if(!_quadrat->isHidden())
//       {
//       _quadrat->hide();
//       _vierecke->hide();
//       }
    }

    if( text == "quadrat")
    { // -1 for not found
//       _quadrat = new QLabel(this);

//       _quadrat->show();
//       _kreis->hide();
//       _vierecke->hide();
        QLabel *label = new QLabel(this);
        label->setFrameStyle(QFrame::Panel | QFrame::Sunken);
        label->setText("first line\nsecond line");
        label->setAlignment(Qt::AlignBottom | Qt::AlignRight);
        label->setGeometry(700,150,30,80);
        label->show();
    }
    else if( text == "vierecke")
    { // -1 for not found
       _vierecke = new QLabel(this);
       _vierecke->show();

    }
    else
    {
        ui->textEdit->setText( " ");
    }

    //qDebug() << index << text << ui->querschnitt_txt->itemData(index, Qt::DisplayRole);
}

void auflagerreaktion::on_pushButton_clicked()
{
//    unsigned i = 0;
//    int l,h,b;

    QString s = ui->querschnitt_txt->currentText();


    if ( s == "kreis")
    { // -1 for not found
       //ui->textEdit->setText( "kreis");
       ui->label_5->setText("kreis");
       QString r= ui->radius_txt->text();
       int r1=r.toInt();
       int A = (22*r1*r1)/7;
       int Umfang = (2*r1*22)/7;
       ui->textEdit->setText("Fläche: "+QString::number(A)+"\n"+"Umfang: "+ QString::number(Umfang));

    }

    else if( s == "quadrat")
    { // -1 for not found
       //ui->textEdit->setText( "quadrat");
       ui->label_5->setText("quadrat");
       QString s = ui->Seitenlaenge_txt->text();
       int s1 = s.toUInt();
       int A = s1 * s1;
       int Umfang = (4*s1);
       ui->textEdit->setText("Fläche: "+QString::number(A)+"\n"+"Umfang: "+ QString::number(Umfang));

    }
    else if( s == "vierecke")
    { // -1 for not found
       ui->textEdit->setText( "vierecke");
       ui->label_5->setText("vierecke");
       QString b = ui->breite_txt->text();
       int b1 = b.toUInt();
       QString h = ui->hoehe_txt->text();
       int h1=h.toUInt();
       int A = b1 * h1;
       int Umfang = (2*b1)+(2*h1);
       ui->textEdit->setText("Fläche: "+QString::number(A)+"\n"+"Umfang: "+ QString::number(Umfang));
    }
    else
    {
        ui->textEdit->setText( " ");
    }



    //charts
#pragma region GRAPGH: first try  {

    /* // problom here that the graph is outside the main widget

    QtCharts::QSplineSeries * series;
    series = new QtCharts::QSplineSeries (this);
    //int la= ui->laenge_txt->text().toin;
    series->append(0, 6);
    series->append(2, 4);
    series->append(3, 8);
    series->append(7, 4);
    series->append(10, 5);
    *series << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);
    QtCharts::QChart *chart;
    chart = new QtCharts::QChart();
    //chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("Simple line chart example");
    QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
    //chartView->setRenderHint(QPainter::Antialiasing);
    //setCentralWidget(chartView);

    QGridLayout* layout= new QGridLayout;
    layout->addWidget(chartView);
    //layout->setSizeConstraint(QLayout::SetFixedSize);
    QWidget* widget = new QWidget(this);
    widget->setLayout(layout);

    widget->setGeometry(70,300,500,500);

    widget->show();
    //setCentralWidget(widget);


    //resize(40, 30);
    //show();
    */

#pragma endregion }



#pragma region GRAPH:  SEC TRY {
// problom hier the graph is not good
/*
 *
    QtCharts::QSplineSeries * series;
    series = new QtCharts::QSplineSeries (this);
    series->append(3, 3);

    //series->append(1, 1);
    //series->append(3, 1);
    //series->append(7, 4);
    //series->append(10, 5);
    *series << QPointF(0, 3) << QPointF(6, 3);// << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);
    QtCharts::QChart *chart;
    chart = new QtCharts::QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->axes(Qt::Vertical).first()->setRange(0, 10);
    chart->axes(Qt::Horizontal).first()->setRange(0, 10);
    chart->setTitle("Simple line chart example");
    QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
    //chartView->setRenderHint(QPainter::Antialiasing);
    //setCentralWidget(chartView);
    ui->gridLayout_3->addWidget(chartView);
    QWidget* widget = new QWidget(this);
    widget->setLayout(ui->gridLayout_3);
    //ui->gridLayout_3->setGeometry(70,300,500,500);
    widget->setGeometry(70,300,500,500);
    widget->show();
    */

#pragma endregion sec_try}

#pragma region GRAPH:  THIRD TRY (new graph){

    // generate some data:
    //double sum=0.0;
//    QVector<float> xax;
//    QVector<float> yax;
//
//    //QVector<double> x(sum), y(sum); // initialize with entries 0..100
//    for (float i=0.0; i<5; i+=0.1)
//    {
//      qDebug()<<"i= "<<i;
//      xax.append((float)i);//100.0 - 1; // x goes from -1 to 1
//      //sum +=(float) i;
//      qDebug()<<"xax[i]= "<<xax[i];
//      double d=(float) xax[i]*xax[i];
//      yax.append((float)d); // let's plot a quadratic function
//      qDebug()<<"yax[i]= "<<yax[i];
//    }
//    for (int i=0.0; i<sizeof (xax); i++)
//    {
//      qDebug()<<"xax[i]= "<<xax[i];
//    }
//    for (int i=0.0; i<sizeof (xax); i++)
//    {
//      qDebug()<<"yax[i]= "<<yax[i];
//    }
//    int sumx= sizeof (xax);
//    int sumy= sizeof (yax);
//    qDebug() << sumx;
//    qDebug() << sumy;
//    QVector<double> x(sumx), y(sumy);
//    //QVector<double> x(sum), y(sum);
//    // create graph and assign data to it:
//    for (double i=0.0; i<6.0; i+=0.1)
//    {
//      x[i] =(double) i-3;//100.0 - 1; // x goes from -1 to 1
//      //sum += i;
//      y[i]=(double) x[i]*x[i];
//      //y[i].append(d); // let's plot a quadratic function
//    }
//    qDebug() << sumx;
//    qDebug() << sumy;
    double le = ui->laenge_txt->text().toDouble();
    double le2 = le* 100;
    qDebug()<<le2;
    QVector<double> x(le2), y(le2); // initialize with entries 0..100
    for (int i=0; i<le2; ++i)
    {
      x[i] = (i+20)/50.0 ;//- 1; // x goes from -1 to 1
      y[i] = x[i]*x[i]; // let's plot a quadratic function
    }
    //QCustomPlot*  customPlot = new QCustomPlot();
    ui->customPlot->addGraph();
    ui->customPlot->graph(0)->setData( x, y);
    // give the axes some labels:
    ui->customPlot->xAxis->setLabel("x");
    ui->customPlot->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    ui->customPlot->xAxis->setRange(0, 10);
    ui->customPlot->yAxis->setRange(0, 10);
    ui->customPlot->replot();
#pragma endregion}

}

//void auflagerreaktion::backToLogIn()
//{
//    qDebug()<<"backtoplogin";
//}




int auflagerreaktion::flaeche(int a,int b,int r)
{
    a=0;
    b=0;
    r=0;
    int fl=0;
    int _kreis = (3,14)*r;
    int quadrat = (a*a);
    int vierecke = a*b;

    return fl;
}

enum class richting {positiv, negativ};
enum class moment_richting {Uhrzeigersinn, gegenUhrzeigersinn};
enum class lager_Art {
    Festlager, Festeinspannung, Freies_Ende
};

class x_kraft
{
    richting _richtung;
    int wert;
};
enum class art_kraft{
    Einzellast, gleichlast, dreiecklast_rechts,dreiecklast_links
};

class y_kraft
{
    richting _richtung;
    int wert;
};
class moment
{
      moment_richting _richtung;
      int wert;
};
class lager
{
    lager_Art _lager;

};
